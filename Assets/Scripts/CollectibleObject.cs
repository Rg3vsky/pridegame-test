﻿using UnityEngine;
using System.Collections;

public class CollectibleObject : MonoBehaviour
{
    [SerializeField]
    private ObjectsEnum objectType;
    [SerializeField]
    private float rotateSpeed = 100;
    public ObjectsEnum Type
    {
        get { return objectType; }
    }
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        this.transform.Rotate(Vector3.forward, rotateSpeed * Time.deltaTime) ;
    }
}
