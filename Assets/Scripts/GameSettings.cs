﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New GameSetting", menuName = "Game Setting", order = 51)]
public class GameSettings : ScriptableObject
{
    [SerializeField]
    [Tooltip("Имя настройки")]
    private string settingName; // Имя настройки
    [SerializeField]
    [Tooltip( "Размер игрового поля" )]
    private short gameFieldSize; // Размер игрового поля
    public short GameFieldSize
    {
        get { return gameFieldSize; }
    }
    [SerializeField]
    [Tooltip( "Максимальное количество объектов на поле" )]
    private short maxAmountOfObjects; // Максимальное количество объектов на поле
    public short MaxAmounOfObjects
    {
        get { return maxAmountOfObjects; }
    }
}
