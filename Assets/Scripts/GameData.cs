﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "NewGameData", menuName = "Game Data", order = 53)]
public class GameData : ScriptableObject
{
    [SerializeField]
    private BasicGameSetting baseSettings;

    public int CurrentPlayerCoinAmount { set; get; }
    public int CurrentPlayerLevel { set; get; }
    public int CurrentPlayerExp { set; get; }
    public int ExpForNextLevel { set; get; }


    private void Awake()
    { 
        InitNewGameData();
    }
    /// <summary>
    /// Вычисление и установка количества опыта для следующего уровня
    /// </summary>
    public void CalculateExpForNewLevel()
    {
        ExpForNextLevel = (int)(baseSettings.BasicExpAmount * CurrentPlayerLevel * baseSettings.ExpMultiplier);
        //Debug.Log( "New XP - " + ExpForNextLevel );
    }
    /// <summary>
    /// Сброс данных при начале новой игры
    /// </summary>
    public void InitNewGameData()
    {
        CurrentPlayerCoinAmount = 0;
        CurrentPlayerLevel = 1;
        CurrentPlayerExp = 0;
        CalculateExpForNewLevel();
    }

}
