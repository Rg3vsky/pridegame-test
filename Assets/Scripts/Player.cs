﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour
{
    [SerializeField]
    private GameManage gameManage;
    [SerializeField]
    private GameData gameData;
    private int currentCoinCount = 0;
    private int currentPlayerLevel = 1;
    private int currentPlayerXP = 0;
    private int nextLevelExp;

    /* Границы передвижения игрока */
    private float minX;
    private float maxX;
    private float minZ;
    private float maxZ;
    
    // Use this for initialization
    void Start()
    {
        UpdatePlayerInfoFromGameData();
    }

    // Update is called once per frame
    void Update()
    {
        if (this.transform.position.x > maxX)
        {
            this.transform.position = new Vector3( maxX, this.transform.position.y, this.transform.position.z );
        }
        if (this.transform.position.x < minX)
        {
            this.transform.position = new Vector3( minX, this.transform.position.y, this.transform.position.z );
        }
        if (this.transform.position.z > maxZ)
        {
            this.transform.position = new Vector3( this.transform.position.x, this.transform.position.y, maxZ );
        }
        if ( this.transform.position.z < minZ )
        {
            this.transform.position = new Vector3( this.transform.position.x, this.transform.position.y, minZ );
        }
    }

    // Проверка, на какой тип объекта наткнулся игрок
    private void OnTriggerEnter(Collider other)
    {
        switch(other.transform.tag)
        {
            case "Coin":
                {
                    gameManage.InteractPickUpButton(true, other.gameObject, ObjectsEnum.Coin);
                    return;
                }
            case "Crystal":
                {
                    gameManage.InteractPickUpButton(true, other.gameObject, ObjectsEnum.Crystal);
                    return;
                }

        }
    }
    // Отключение кнопки сбора предмета при удалении от него
    private void OnTriggerExit(Collider other)
    {
        gameManage.InteractPickUpButton();
    }
    // Обновление данных игрока из хранилища
    public void UpdatePlayerInfoFromGameData()
    {
        currentPlayerXP = gameData.CurrentPlayerExp;
        if (gameData.CurrentPlayerExp >= gameData.ExpForNextLevel)
        {
            gameData.CurrentPlayerExp -= gameData.ExpForNextLevel;
            currentPlayerXP = gameData.CurrentPlayerExp;
            gameData.CurrentPlayerLevel++;
            gameData.CalculateExpForNewLevel();
            gameManage.LevelUp();
        }
        nextLevelExp = gameData.ExpForNextLevel;
        currentCoinCount = gameData.CurrentPlayerCoinAmount;
        currentPlayerLevel = gameData.CurrentPlayerLevel;
    }

    /// <summary>
    /// Установка границ движения в зависимости от размера игрового поля
    /// </summary>
    public void UpdatePositionLimits()
    {
        maxX = maxZ = ( float )gameManage.levelSetting.GameFieldSize;
        minX = minZ = ( float )gameManage.baseSetting.GameCell.transform.localScale.x / 2.0f;
    }
}
