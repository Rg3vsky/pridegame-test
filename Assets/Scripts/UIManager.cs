﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIManager : MonoBehaviour
{
    [Header( "Счетчики" )]
    [Tooltip( "Монеты" )]
    public Text coinText;
    [Tooltip( "Уровень" )]
    public Text levelText;
    [Tooltip( "Опыт" )]
    public Text xpText;
    [Tooltip( "Полоска опыта" )]
    public Image xpBar;
    [Header("Кнопки")]
    [Tooltip( "Подъем предмета" )]
    public Button pickUpButton;
    [Header( "Панели" )]
    [Tooltip( "Панель -Новый уровень-" )]
    public GameObject levelUpPanel;
    private CanvasGroup levelUpAlfa;
    private GameManage gameManage;
    [SerializeField]
    [Header( "Вспомогательная секция" )]
    private GameData gameData;
    // Use this for initialization
    void Start()
    {
        gameManage = FindObjectOfType<GameManage>();
        levelUpAlfa = levelUpPanel.GetComponent<CanvasGroup>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    /// <summary>
    /// Вешается на кнопку в зависимости от типа объекта. Подбор объекта
    /// </summary>
    /// <param name="objType">Тип подбираемого объекта</param>
    /// <param name="objToRemove">Сам объект</param>
    /// <param name="value">Значение</param>
    public void PickUpObject(ObjectsEnum objType, GameObject objToRemove, int value = 1)
    {
        switch (objType)
        {
            case ObjectsEnum.Coin:
                {
                    gameData.CurrentPlayerCoinAmount += value;
                    coinText.text = gameData.CurrentPlayerCoinAmount.ToString();
                    gameManage.CoinCollected(objToRemove); // вызов функции удаления объекта
                    pickUpButton.onClick.RemoveAllListeners();
                    pickUpButton.interactable = false;
                    return;
                }
            case ObjectsEnum.Crystal:
                {
                    gameData.CurrentPlayerExp += value;
                    xpText.text = gameData.CurrentPlayerExp + "/" + gameData.ExpForNextLevel;
                    xpBar.fillAmount = (float)gameData.CurrentPlayerExp / (float)gameData.ExpForNextLevel;
                    gameManage.CrystalCollected( objToRemove ); // вызов функции удаления объекта
                    pickUpButton.onClick.RemoveAllListeners();
                    pickUpButton.interactable = false;
                    return;
                }
        }
    }
    // Обновление информации об уровне игрока на UI 
    public void LevelUp()
    {
        StartCoroutine( showLevelUpPanel() );
        levelText.text = gameData.CurrentPlayerLevel.ToString();
        xpText.text = gameData.CurrentPlayerExp + "/" + gameData.ExpForNextLevel;
        xpBar.fillAmount = ( float )gameData.CurrentPlayerExp / ( float )gameData.ExpForNextLevel;
    }
    // Плавное появление панели
    private IEnumerator showLevelUpPanel()
    {
        while(levelUpAlfa.alpha < 1)
        {
            levelUpAlfa.alpha += 0.1f;
            yield return new WaitForSeconds( 0.1f );
        }
        StartCoroutine( HideLevelUpPanel() );
    }
    // Плавное затухание панели
    private IEnumerator HideLevelUpPanel()
    {
        while(levelUpAlfa.alpha > 0)
        {
            levelUpAlfa.alpha -= 0.1f;
            yield return new WaitForSeconds( 0.1f );
        }
    }
}
