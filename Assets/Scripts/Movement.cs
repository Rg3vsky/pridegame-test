﻿using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour
{
    private GameManage gameManage;
    private void Start()
    {
        gameManage = FindObjectOfType<GameManage>();
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.W))
        {
            this.transform.localPosition += this.transform.forward * gameManage.baseSetting.MoveMultiplier * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.S))
        {
            this.transform.localPosition -= this.transform.forward * gameManage.baseSetting.MoveMultiplier * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.A))
        {
            this.transform.Rotate(Vector3.down, gameManage.baseSetting.RotateMultiplier * Time.deltaTime);
            //this.transform.localPosition += Vector3.left * gameManage.baseSetting.MoveMultiplier * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.D))
        {
            this.transform.Rotate(Vector3.up, gameManage.baseSetting.RotateMultiplier * Time.deltaTime);
        }
    }
}
