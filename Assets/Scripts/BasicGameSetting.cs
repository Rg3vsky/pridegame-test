﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "Basic GameSettings", menuName = "Base Game Setting", order = 52)]
public class BasicGameSetting : ScriptableObject
{
    [Header("Основные элементы игры")]
    [SerializeField]
    [Tooltip( "Префаб клетки игрового поля" )]
    private GameObject gameCellPrefab; // Префаб клетки игрового поля
    public GameObject GameCell
    {
        get { return gameCellPrefab; }
    }
    [SerializeField]
    [Tooltip( "Префаб монеты" )]
    private GameObject coinPrefab; // Префаб монеты
    public GameObject Coin
    {
        get { return coinPrefab; }
    }
    [SerializeField]
    [Tooltip( "Префаб кристалла" )]
    private GameObject crystalPrefab; // Префаб кристалла
    public GameObject Crystal
    {
        get { return crystalPrefab; }
    }
    [SerializeField]
    [Tooltip( "Минимальное количество объектов на поле" )]
    private short minObjectSpawnAmoun = 2;  // Минимальное количество объектов для спавна
    public short MinAmountOfObj
    {
        get { return minObjectSpawnAmoun; }
    }
    [SerializeField]
    [Range(0.1f, 0.9f)]
    [Tooltip( "Процент соотношения монет к кристаллам" )]
    private float percentageOfCoins = 0.5f; // Соотношение монеты/кристалы при спавне, при нечетных числах монет будет больше чем кристаллов
    public float PercentageOfCoins
    {
        get { return percentageOfCoins; }
    }
    [SerializeField]
    [Tooltip( "Модификатор скорости игрока" )]
    private float moveSpeedMultiplier = 1f; // Модификатор скорости перемещения игрока
    public float MoveMultiplier
    {
        get { return moveSpeedMultiplier; }
    }
    [SerializeField]
    [Tooltip( "Модификатор скорости поворота" )]
    private float rotateSpeedMultiplier; // Модификатор скорости поворота игрока
    public float RotateMultiplier
    {
        get { return rotateSpeedMultiplier; }
    }
    [SerializeField]
    [Tooltip( "Список всех настроек игры" )]
    private List<GameSettings> allAvailableSettings; // Список возможных настроек игры

    [Header("Настройки логики игры")]
    [SerializeField]
    [Tooltip("Количество опыта за 1 кристалл")]
    private int amountOfExpForCrystal = 20; // Количество опыта за один кристалл
    public int AmountExpForCrytal
    {
        get { return amountOfExpForCrystal; }
    }
    [SerializeField]
    [Tooltip("Множитель увеличения опыта для следующего уровня")]
    private float expMultiplier = 1.0f; // Множитель опыта для получения следующего уровня
    public float ExpMultiplier
    {
        get { return expMultiplier; }
    }
    [SerializeField]
    [Tooltip("Базовое количество опыта для уровня")]
    private int basicExpAmount = 100; // Базовое количество опыта для получения нового уровня
    public int BasicExpAmount
    {
        get { return basicExpAmount; }
    }
    /// <summary>
    /// Возвращает случайную настройку из списка настроек игры
    /// </summary>
    /// <returns></returns>
    public GameSettings GetRandomLevelSetting()
    {
        int rand = Random.Range(0, allAvailableSettings.Count);
        return allAvailableSettings[rand];
    }
}
