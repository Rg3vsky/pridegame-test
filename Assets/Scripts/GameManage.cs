﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManage : MonoBehaviour
{
    // Основная секция
    public GameObject fieldContainer; //  Контейнер для игрового поля
    public GameSettings levelSetting; // Настройки игры для текущего раунда
    public BasicGameSetting baseSetting; // Базовые настройки игры
    private Vector3 baseCellSpawnPosition = new Vector3(0, -0.1f, 0); // Базовая позиция для генерации поля
    // Секция объектов
    private List<GameObject> spawnedObjects = new List<GameObject>(); // Список всех созданных объектов (монет и кристаллов)
    private bool[,] occupiedCellsArray; // массив с булами для определения занята ли клетка каким либо объектом МАССИВ[X, Z]
    private short amountOfObjectOnLevel; // Количество объектов в игре
    // Секция вспомогательная
    [SerializeField]
    private UIManager uiManager; // Для взаимодействия с интерфесом игры
    [SerializeField]
    private Player player; // Используется для взаимодействия с игроком
    private Vector3 basePlayerPosition; // Базовая позиция игрока
    [SerializeField]
    private GameData gameData; // Данные игры для прочтения
    

    // Start is called before the first frame update

    void Start()
    {
        gameData.InitNewGameData();   
        LevelInit();
        basePlayerPosition = player.gameObject.transform.position;
        player.UpdatePositionLimits();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    /// <summary>
    /// Генерация уровня
    /// </summary>
    public void LevelInit()
    {
        levelSetting = baseSetting.GetRandomLevelSetting();
        occupiedCellsArray = new bool[levelSetting.GameFieldSize, levelSetting.GameFieldSize];
        amountOfObjectOnLevel = (short)Random.Range(baseSetting.MinAmountOfObj, levelSetting.MaxAmounOfObjects + 1);

        for (int i = 0; i < levelSetting.GameFieldSize; i++)
        {
            for (int j = 0; j < levelSetting.GameFieldSize; j++)
            {
                GameObject cell = Instantiate(baseSetting.GameCell, fieldContainer.transform);
                cell.transform.localPosition = baseCellSpawnPosition;
                cell.name = "CELL Z-" + i + ": X-" + j;
                baseCellSpawnPosition.x += baseSetting.GameCell.transform.localScale.x;
                occupiedCellsArray[j, i] = false;
            }
            baseCellSpawnPosition.x = 0;
            baseCellSpawnPosition.z += baseSetting.GameCell.transform.localScale.z;
        }
        baseCellSpawnPosition.z = 0;
        SpawnObjects();
    }

    /// <summary>
    /// Генерация объектов на уровне
    /// </summary>
    public void SpawnObjects()
    {
        short amountOfCoins;
        short amountOfCrystals;
        if (amountOfObjectOnLevel == baseSetting.MinAmountOfObj)
        {
            amountOfCoins = 1;
            amountOfCrystals = 1;
        }
        else
        {
            amountOfCoins = (short)Mathf.Ceil(amountOfObjectOnLevel * baseSetting.PercentageOfCoins);
            amountOfCrystals = (short)(amountOfObjectOnLevel - amountOfCoins);
        }
        // Спавним монеты
        int i = amountOfCoins;
        while(i > 0)
        {
            int randX = Random.Range(0, levelSetting.GameFieldSize);
            int randZ = Random.Range(0, levelSetting.GameFieldSize);
            if (!occupiedCellsArray[randX, randZ])
            {
                GameObject coinObj = Instantiate(baseSetting.Coin, fieldContainer.transform);
                coinObj.transform.localPosition = new Vector3(randX, 0.5f, randZ);
                spawnedObjects.Add(coinObj);
                i--;
                occupiedCellsArray[randX, randZ] = true;
                
            }
           
        }
        // Спавним кристаллы
        int j = amountOfCrystals;
        while (j > 0)
        {
            int randX = Random.Range(0, levelSetting.GameFieldSize);
            int randZ = Random.Range(0, levelSetting.GameFieldSize);
            if (!occupiedCellsArray[randX, randZ])
            {
                GameObject crystalObj = Instantiate(baseSetting.Crystal, fieldContainer.transform);
                crystalObj.transform.localPosition = new Vector3(randX, 0.5f, randZ);
                spawnedObjects.Add(crystalObj);
                j--;
                occupiedCellsArray[randX, randZ] = true;
            }
        }
    }
    
    /// <summary>
    /// Удаляет объект с поля и выполняет проверку на завершение уровня
    /// </summary>
    /// <param name="coinObj"></param>
    public void CoinCollected(GameObject coinObj)
    {
        player.UpdatePlayerInfoFromGameData();
        spawnedObjects.Remove(coinObj);
        Destroy(coinObj);
        if (spawnedObjects.Count == 0)
        {
            LevelComplete();
        }
    }

    /// <summary>
    /// Удаляет объект с поля и выполняет проверку на завершение уровня
    /// </summary>
    /// <param name="crystalObj"></param>
    public void CrystalCollected(GameObject crystalObj)
    {
        player.UpdatePlayerInfoFromGameData();
        spawnedObjects.Remove(crystalObj);
        Destroy(crystalObj);
        if (spawnedObjects.Count == 0)
        {
            LevelComplete();
        }
    }

    /// <summary>
    /// Повышение уровня игрока
    /// </summary>
    public void LevelUp()
    {
        uiManager.LevelUp();
        player.UpdatePlayerInfoFromGameData();
    }
    /// <summary>
    /// Завершение уровня и установка игрока на стартовую локацию
    /// </summary>
    public void LevelComplete()
    {
        ClearLevel();
        LevelInit();
        player.transform.position = basePlayerPosition;
        player.UpdatePositionLimits();
    }
    /// <summary>
    /// Очистка уровня
    /// </summary>
    private void ClearLevel()
    {
        for (int i = fieldContainer.transform.childCount; i > 0; i-- )
        {
            Destroy( fieldContainer.transform.GetChild( i - 1 ).gameObject );
        }
    }
    /// <summary>
    /// Используется для отключении кнопки PickUp (конечно можно было бы и отдельную функцию)
    /// </summary>
    /// <param name="isEnable"></param>
    public void InteractPickUpButton(bool isEnable = false)
    {
        if (!isEnable)
        {
            uiManager.pickUpButton.onClick.RemoveAllListeners();
            uiManager.pickUpButton.interactable = isEnable;
        }
    }

    /// <summary>
    /// Включает кнопку PickUp и прописывает ей определённую функцию в зависимости от переданного типа объекта
    /// </summary>
    /// <param name="isEnable"></param>
    /// <param name="collectibleObj">Передаваемый объект</param>
    /// <param name="objType">Тип передаваемого объекта</param>
    public void InteractPickUpButton(bool isEnable, GameObject collectibleObj, ObjectsEnum objType)
    {
        uiManager.pickUpButton.interactable = isEnable;
        switch (objType)
        {
            case ObjectsEnum.Coin:
                {
                    uiManager.pickUpButton.onClick.AddListener(() => uiManager.PickUpObject(objType, collectibleObj));
                    return;
                }
            case ObjectsEnum.Crystal:
                {
                    uiManager.pickUpButton.onClick.AddListener(() => uiManager.PickUpObject(objType, collectibleObj, baseSetting.AmountExpForCrytal));
                    return;
                }
        }

        
    }
}
